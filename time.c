#include <reg52.h>                      /* 包含头文件 */
#define uint		unsigned int
#define uchar		unsigned char
#define schar		signed char

/*****************LED灯对应P0口的1个端口*************/
sbit LED1 = P1^0;
sbit LED2 = P1^1;
sbit LED3 = P1^2;
sbit LED4 = P1^3;

/*****************相关变量**************/
uchar Receive, Receive_table[15];   
uint  i = 0;	 //计时辅助变量 每当定时器中断触发一次 加1


/********************功能函数***************************/
//串口发送数据函数
void Send_Uart(uchar c)
{
    ES = 0;						/* 关串口中断 */
    SBUF = c;
    while(!TI);
    TI = 0;
	ES = 1;
}

//延时函数
void ms_delay( uint t )
{
	uint k, l;
	for ( k = t; k > 0; k-- )
		for ( l = 110; l > 0; l-- );
}


void us_delay( uchar x )
{
	while ( x-- );
}
/*******************二级函数*************************/
//WIFI模块设置函数
void ESP8266_Set( uchar *puf )    /* 数组指针*puf指向字符串数组 */
{
	while ( *puf != '\0' )        /* 遇到空格跳出循环 */
	{   
	    Send_Uart( *puf );        /* 向WIFI模块发送控制指令。 */
		puf++;
	}
	us_delay( 5 );
	Send_Uart( '\r' );             /* 回车 */
	us_delay( 5 );
	Send_Uart( '\n' );             /* 换行 */
	ms_delay( 1000 );
}
//WIFI模块连接透传云函数
void ESP8266_Secret( uchar *puff )    /* 数组指针*puf指向字符串数组 */
{
	do 
	{   
	    Send_Uart( *puff );       
		puff++;
	}
	while( *puff != '\0' ) ;
}
/**********************总中断 控制*************************/
void Allinterrupt(void)
{
  EA =1;      // 开总中断	 
  IP = 0x10;  // 中断优先级设置  串口中断 高于 定时器中断
  ES = 1;     // 开串口中断
}

/**************定时器中断 T0定时器 方式1 延时0.05s********************/
void InitTimer0(void)
{
//	TMOD = 0x21;
    TH0 = 0x4C;
    TL0 = 0x00;
    ET0 = 1; 
    TR0 = 1;
}

/****************串口中断 T1定时器 方式2 波特率9600*************/
void InitUART(void)
{
    TMOD = 0x21;
    SCON = 0x50;
    TH1 = 0xFD;
    TL1 = TH1;
    PCON = 0x00;
//  ES = 0;		//关串口中断   目的：在T1定时器触发的中断运行函数中发送的串口数据不采用中断	 -》》》发现不采用中断不行，中
    TR1 = 1;
}
/************************************************************************/
/**********************************主函数********************************/
/************************************************************************/
void main(void)
{	
    LED1 = 0;
    ms_delay( 2000 );
    InitUART();	    //串口波特率兼中断初始化  此时未开启EA总中断  发送时不需要中断
	ESP8266_Set( "AT+CWMODE=1" ); 	  //设置路由器模式 1 station模式 2 AP  路由器模式 3 station+AP混合模式
	ms_delay( 100 );
	ESP8266_Set( "AT+CWJAP=\"xuee\",\"20090201\"" );
	ms_delay( 8000 );
	ESP8266_Set( "AT+CIPSTART=\"TCP\",\"115.29.240.46\",9000" ); 
	ESP8266_Set( "AT+CIPMODE=1" );		     
	ESP8266_Set( "AT+CIPSEND" );                         
	ESP8266_Secret( "ep=181U4ZSY29TYNX6F&pw=esp001" );
	ms_delay( 100 );
	Allinterrupt(); //总中断控制
	InitTimer0();   //定时器中断初始化								
	while(1)
	{
		if( Receive_table[1] == '1' )
		{
			LED2 = 1;    // 1 灯灭 

		}
		if( Receive_table[1] == '0' )
		{
			LED2 = 0;     // 0 灯亮

		}
		if( Receive_table[1] == 'R' )
		{
			LED3 = 0;    // 0 灯亮 
			ms_delay( 3000 );
			LED3 = 1;
			Receive_table[1]='+';
		}
	}
}

/***************定时器中断*****************/
void Timer0Interrupt(void) interrupt 1
{	
    //重新装填计时值
    TH0 = 0x4C;
    TL0 = 0x00;
    //add your code here!
	if(i<1200)
	{
	   i++;
	}else{
	   i=0;
	   LED4 = 0; 
	   ESP8266_Secret( "+++" );
	   ms_delay( 2000 );
	   ESP8266_Set( "AT+PING=\"180.76.76.76\"" );
	   ms_delay( 20000 );		     
	   LED4 = 1; 
	}
}

/****************串口中断******************/
void UARTInterrupt(void) interrupt 4
{	static uchar j =0 ;
    if(RI)
    {
        RI = 0;
        //add your code here!
		Receive	= SBUF; /* MCU接收wifi模块反馈回来的数据 */
		//因为ping时返回的timeout  前面有+  另外还可以作为 新指令的开始标志进行检测
        if(Receive == '+')
		{ 
		   j = 0;
		}
	    // 当指令中的数据包含  换行 回车 空字符 或是j>15时 不向数组中写入数据
		if((Receive != '\n')&&(Receive != '\r')&&(Receive != '\0')&&(j < 15))
		{
			Receive_table[j] = Receive;
			j++;
		}else{
			j = 0;
		}
		
    }
   	if (TI)
	{
	    TI = 0;
	}
}




