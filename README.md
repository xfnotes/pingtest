# 网络PING检测断网自动断电重启装置

#### 介绍
通过esp8266检测网络通断，当网络ping不通时将控制继电器断电，数秒之后自动接通继电器。若是连接到路由器上可以用来守护路由器

#### 软件架构
软件架构说明


#### 安装教程

1. 焊接电路板根据图纸
2. 烧录hex文件

#### 使用说明

1. C文件为源文件通过keil编译可为hex
2. 初级版本，就是为了应付学校比赛，各类电器保护装置，冗余运行设置都没有

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)